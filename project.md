# WebHelper

## Create group _sftp_

```shell
sudo groupadd sftp
```

## Edit _/etc/ssh/sshd_config_

```config
PasswordAuthentication no
ChallengeResponseAuthentication no

UsePAM no
Subsystem sftp internal-sftp
Match Group sftp
        ChrootDirectory %h
        AllowTCPForwarding no
        ForceCommand internal-sftp
        RSAAuthentication yes
        PubkeyAuthentication yes
        AuthorizedKeysFile %h/.ssh/authorized_keys
        PasswordAuthentication no
```

## create www folder

```shell
mkdir /var/www
chown root:root /var/www
chmod 755 /var/www
```

## Depends

-   certbot(letsencrypt)
-   nginx >= 1.9.5
-   openssl
-   crontab

## _Nginx_ configuration

```shell
mkdir /etc/nginx/{sites-available,sites-enabled}
```

**sites-available** - for site configuration  
**sites-enabled**   - for enable configuration (symlink)

add to nginx.conf in http section

```config
     include /etc/nginx/sites-enabled/*;
```

## If you use iptables

Replace 22 by your port sftp

```config
-A INPUT -m state --state NEW -p tcp -m multiport --dport 22,80,443 -j ACCEPT
```

## Generate dhparam

```bash
openssl dhparam -out /etc/nginx/dhparam.pem 4096
```

## E-mail conf

For let's encript need e-mail: admin@**domain.com**

---

---

---

### Path(путь)

Берётся из параметров передаваемых программе, если нету то берётся из
конфигурационного файла. Если и там его нет или он пуст,
то по умолчанию `/var/www`
