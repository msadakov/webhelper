package main

import (
	"fmt"
	"os/exec"
)

func initialСonf(domainName string) {

	initialСonfContent := `
server {
    listen 80;
    server_name ` + domainName + `;
    root /var/www/` + domainName + `/www;
}
`
	fmt.Println("Initial Nginx configuration")
	createFile("/etc/nginx/sites-available/"+domainName, initialСonfContent)
	fmt.Println("Enable Nginx configuration")
	createSymlink("/etc/nginx/sites-available/"+domainName, "/etc/nginx/sites-enabled/"+domainName)
	// reload nginx
	systemctlPath, _ := exec.LookPath("systemctl")
	fmt.Println("Reload conf Nginx")
	exec.Command(systemctlPath, "reload", "nginx").Run()
}

func letsencrypt(domainName string) {
	fmt.Println("Create certificates")
	certbotPath, _ := exec.LookPath("certbot")
	// out, _ := exec.Command(certbotPath, "certonly", "--test-cert", "-n", "--agree-tos", "--email", "admin@"+domainName, "--webroot", "-w", "/var/www/"+domainName+"/www", "-d", domainName).Output()
	// fmt.Println(string(out))
	exec.Command(certbotPath, "certonly", "--renew-by-default", "-n", "--agree-tos", "--email", "admin@"+domainName, "--webroot", "-w", "/var/www/"+domainName+"/www", "-d", domainName).Run()
}

func updateConf(domainName string) {
	fmt.Println("Update Nginx conf with http2, https")

	initialСonfContent := `
# redirect http to https no-www
server {
    server_name          ` + domainName + `;
    listen               *:80;
    listen               [::]:80;
    return 301 https://` + domainName + `$request_uri;
}

# main
server {
    server_name          ` + domainName + `;
    listen               *:443 ssl http2;
    listen               [::]:443 ssl http2;

    keepalive_timeout   70;
    ssl on;

    # Certs
    ssl_certificate /etc/letsencrypt/live/` + domainName + `/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/` + domainName + `/privkey.pem;

    # to reduce the load
    ssl_session_timeout 1d;
    ssl_session_cache shared:SSL:10m;

    # dh
    ssl_dhparam /etc/nginx/dhparam.pem;

    # Unsafe and disable anonymous ciphers
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS';
    # allow the use only these ciphers us
    ssl_prefer_server_ciphers on;

    # HSTS (ngx_http_headers_module is required) (15768000 sec = 6 months, 31536000 sec = 1 year)
    add_header Strict-Transport-Security max-age=15768000;

    # OCSP| For decreasing load time of pages that allow the server to validate the certificate to attach OCSP responses
    ssl_stapling on;
    ssl_stapling_verify on;
    ssl_trusted_certificate /etc/letsencrypt/live/` + domainName + `/chain.pem;

    # DNS for OSCP
    resolver 8.8.8.8 8.8.4.4 valid=86400;
    resolver_timeout 10;

    # prohibition of the show site in frame
    add_header X-Frame-Options DENY;

    # Specify the browser to use the given server Сontent-type, instead of automatically determining it
    add_header X-Content-Type-Options nosniff;

    #  XSS protection
    add_header X-XSS-Protection "1; mode=block";

    charset UTF-8;
    index index.html;
    root /var/www/` + domainName + `/www;
    access_log /var/www/` + domainName + `/log/access.log ;
    error_log /var/www/` + domainName + `/log/error.log notice;

    # ....
}
`
	createFile("/etc/nginx/sites-available/"+domainName, initialСonfContent)
	// reload nginx
	systemctlPath, _ := exec.LookPath("systemctl")
	fmt.Println("Reload conf Nginx")
	exec.Command(systemctlPath, "reload", "nginx").Run()
}
