package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strconv"
)

// Myexternalip represents the ip address of the server
type Myexternalip struct {
	IP string `json:"ip"`
}

// YaAPIPDD for add, remove and list
type YaAPIPDD struct {
	Domain  string `json:"domain"`
	Records []struct {
		RecordID  int    `json:"record_id"`
		Subdomain string `json:"subdomain"`
	} `json:"records"`
	Success string `json:"success"`
	Error   string `json:"error"`
}

// Checks domain existence
func checkDomain(domain string) bool {
	_, err := net.LookupHost(domain)
	if err != nil {
		return false
	}
	return true
}

// Function to retrieve external IP address in Internet
func meip(ipv string) string {
	meiphttp, err := http.Get("http://ipv" + ipv + ".myexternalip.com/json")
	if err != nil {
		printErrorAndExit(fmt.Errorf("Can't connect to myexternalip.com"))
	}
	meipJSON, _ := ioutil.ReadAll(meiphttp.Body)

	meiphttp.Body.Close()

	jsonSrc := []byte(meipJSON)

	var meip Myexternalip
	json.Unmarshal(jsonSrc, &meip)

	return meip.IP
}

// Function for interacting with the API Yandex Mail for domain
func yaDNSAPI(method, url, token string) (YaAPIPDD, error) {
	req, err := http.NewRequest(method, url, nil)
	req.Header.Set("PddToken", token)

	client := &http.Client{}
	yaapihttp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer yaapihttp.Body.Close()
	var yaapi YaAPIPDD

	yaapiJSON, err := ioutil.ReadAll(yaapihttp.Body)
	if err != nil {
		return yaapi, fmt.Errorf("Can't create DNS record: %v", err)
	}

	json.Unmarshal([]byte(yaapiJSON), &yaapi)

	return yaapi, nil
}

// Adds a DNS entry to Yandex Mail for domain
func yaDNSAdd(domain, subdomain, typer, ip, token string) error {
	domainAddURL := "https://pddimp.yandex.ru/api2/admin/dns/add?domain=" + domain + "&type=" + typer + "&subdomain=" + subdomain + "&content=" + ip + "&test=test"
	domainAddJSON, err := yaDNSAPI("POST", domainAddURL, token)

	if err != nil {
		return err
	}

	if domainAddJSON.Success == "error" {
		err = fmt.Errorf("Can't create DNS record: %v", domainAddJSON.Error)
	}

	return err
}

// The function first gets the entire list of records, then look for and remove it
func yaDNSDel(domain, subdomain, token string) (err error) {

	domainListURL := "https://pddimp.yandex.ru/api2/admin/dns/list?domain=" + domain
	domainListJSON, err := yaDNSAPI("GET", domainListURL, token)
	if err != nil {
		return err
	}

	for _, record := range domainListJSON.Records {
		if record.Subdomain == subdomain {
			domainDelURL := "https://pddimp.yandex.ru/api2/admin/dns/del?domain=" + domain + "&record_id=" + strconv.Itoa(record.RecordID)
			domainDelJSON, err := yaDNSAPI("POST", domainDelURL, token)
			if err != nil {
				return err
			}
			if domainDelJSON.Success == "error" {
				return fmt.Errorf("Can't remove DNS record: %v", domainDelJSON.Error)
			}
		}
	}

	return
}

// If the IP address is empty it is taken from the Internet
func autoDetectIP(ipv4, ipv6 string) (string, string) {
	if ipv4 == "" {
		ipv4 = meip("4")
	}
	if ipv6 == "" {
		ipv6 = meip("6")
	}
	return ipv4, ipv6
}

func addDomain(dns, domain, subdomain, ipv4, ipv6, token string) error {
	var err error

	if dns == "yandex" || dns == "ya" {
		ipv4, ipv6 = autoDetectIP(ipv4, ipv6)

		if err = yaDNSAdd(domain, subdomain, "A", ipv4, token); err != nil {
			return err
		}

		if ipv6 != "false" && ipv6 != "" {
			if err = yaDNSAdd(domain, subdomain, "AAAA", ipv6, token); err != nil {
				return err
			}
		}
	} else {
		err = fmt.Errorf("Incorrect DNS service")
	}

	return err
}

func delDomain(dns, domain, subdomain, token string) error {
	var err error

	if dns == "yandex" || dns == "ya" {
		if err = yaDNSDel(domain, subdomain, token); err != nil {
			return err
		}
	} else {
		err = fmt.Errorf("Incorrect DNS service")
	}

	return err

}
