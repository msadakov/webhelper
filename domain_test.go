package main

import "testing"

func Test_Domain_CheckDomain(t *testing.T) {
	cases := []struct {
		in   string
		want bool
	}{
		{"google.ru", true},
		{"webhelper.google.com", false},
	}
	for _, c := range cases {
		got := checkDomain(c.in)
		if got != c.want {
			t.Errorf("CheckDomain(%q) == %v, want %v", c.in, got, c.want)
		}
	}
}

// func Test_Domain_Meip(t *testing.T) {
// 	cases := []struct {
// 		ipv  string
// 		want string
// 	}{
// 		{"4", ""},
// 		{"6", ""},
// 	}
//
// 	for _, c := range cases {
// 		got := meip(c.ipv)
// 		if got != c.want {
// 			t.Errorf("meip(%q) == %q, want %q", c.ipv, got, c.want)
// 		}
// 	}
// }

// func Test_Domain_addDomain(t *testing.T) {
// 	readConfigFile("my.config")
// 	cases := []struct {
// 		dns, domain, subdomain, ipv4, ipv6 string
// 		want                               error
// 	}{
// 		{"yandex", "dev-m.ru", "webhelper", "", "", nil},
// 	}
//
// 	for _, c := range cases {
// 		got := addDomain(c.dns, c.domain, c.subdomain, c.ipv4, c.ipv6, config.Webhelper.Token)
// 		if got != c.want {
// 			t.Errorf("addDomain(%q,%q,%q,%q,%q,%q) == %v, want %v", c.dns, c.domain, c.subdomain, c.ipv4, c.ipv6, config.Webhelper.Token, got, c.want)
// 		}
// 	}
// }

// func Test_Domain_delDomain(t *testing.T) {
// 	readConfigFile("my.config")
// 	cases := []struct {
// 		dns, domain, subdomain string
// 		want                   error
// 	}{
// 		{"yandex", "dev-m.ru", "webhelper", nil},
// 	}
//
// 	for _, c := range cases {
// 		got := delDomain(c.dns, c.domain, c.subdomain, config.Webhelper.Token)
// 		if got != c.want {
// 			t.Errorf("delDomain(%q,%q,%q,%q) == %v, want %v", c.dns, c.domain, c.subdomain, config.Webhelper.Token, got, c.want)
// 		}
// 	}
// }
