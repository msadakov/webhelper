package main

import "testing"

// func Test_Linux_CheckSuperUser(t *testing.T) {
// 	cases := []struct {
// 		want bool
// 	}{
// 		{false},
// 	}
// 	for _, c := range cases {
// 		got := checkSuperUser()
// 		if got != c.want {
// 			t.Errorf("CheckSuperUser() == %v, want %v", got, c.want)
// 		}
// 	}
// }

// func Test_Linux_AddUserAndSiteFolders(t *testing.T) {
// 	cases := []struct {
// 		domainName, userName, sitesPath string
// 		sftp                            bool
// 	}{
// 		{"test.example", "", "/var/www", true},
// 		{"test2.example", "", "/var/www", true},
// 	}
// 	for _, c := range cases {
// 		uid, gid := addUser(c.domainName, c.userName, c.sitesPath, c.sftp)
//
// 		addSiteFolders(c.domainName, uid, gid)
// 	}
// }

// func Test_Linux_getUser(t *testing.T) {
// 	cases := []struct {
// 		in, want string
// 	}{
// 		{"testexample", "testexample"},
// 		{"test2example", "test2example"},
// 	}
// 	for _, c := range cases {
// 		got := getUser(c.in)
// 		if got.Username != c.want {
// 			t.Errorf("getUser(%q) == %q, want %q", c.in, got.Username, c.want)
// 		}
// 	}
// }

func Test_Linux_CreateFile(t *testing.T) {
	cases := []struct {
		in1, in2 string
	}{
		{"/var/www/test.example/www/index.html", "<!DOCTYPE html><html lang=\"en\"><meta charset=\"UTF-8\" /><title>Document</title>WebHelper!</html>"},
		{"/var/www/test2.example/www/index.html", "<!DOCTYPE html><html lang=\"en\"><meta charset=\"UTF-8\" /><title>Document</title>WebHelper!</html>"},
	}
	for _, c := range cases {
		createFile(c.in1, c.in2)
	}
}

func Test_Linux_RemovePath(t *testing.T) {
	cases := []struct {
		in   string
		want error
	}{
		{"/var/www/test.example", nil},
		{"/var/www/test2.example", nil},
	}
	for _, c := range cases {
		got := removePath(c.in)
		if got != c.want {
			t.Errorf("RemovePath(%q) == %q, want %q", c.in, got, c.want)
		}
	}
}

// func Test_Linux_removeUser(t *testing.T) {
// 	cases := []struct {
// 		in   string
// 		want error
// 	}{
// 		{"testexample", nil},
// 		{"test2example", nil},
// 	}
// 	for _, c := range cases {
// 		got := removeUser(c.in)
// 		if got != c.want {
// 			t.Errorf("removeUser(%q) == %q, want %v", c.in, got, c.want)
// 		}
// 	}
// }
