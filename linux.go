package main

import (
	"crypto/rand"
	"fmt"
	"io"
	"os"
	"os/exec"
	"os/user"
	"regexp"
	"strconv"
)

// CheckSuperUser checks whether the current user is a superuser privileges
func checkSuperUser() bool {
	userInfo, err := user.Current()
	if err != nil {
		panic(err)
	}
	if userInfo.Uid == "0" && userInfo.Gid == "0" {
		return true
	}
	return false
}

func createDir(homeDir string) bool {
	_, err := os.Stat(homeDir)
	if err != nil {
		//home dir does not exist
		err := os.MkdirAll(homeDir, 0755)
		if err != nil {
			return false
		}
		return true
	}
	return true
}

func createUser(domainName, userName, sitesPath string, sftp bool) {
	userPassword := newPassword(32)
	cmdUserAddPath, _ := exec.LookPath("useradd")
	//run command for create user
	// if err := exec.Command(cmdUserAddPath, "-d", sitesPath+domainName, "-s", "/bin/false", "-G", "sftp", "-p", userPassword, userName).Run(); err != nil {
	// 	return fmt.Errorf("Can't create user: %v", err)
	// }
	var (
		out []byte
		err error
	)

	if sftp {
		out, err = exec.Command(cmdUserAddPath, "-d", sitesPath+"/"+domainName, "-s", "/bin/false", "-G", "sftp", "-p", userPassword, userName).CombinedOutput()
	} else {
		out, err = exec.Command(cmdUserAddPath, "-d", sitesPath+"/"+domainName, "-s", "/bin/false", "-p", userPassword, userName).CombinedOutput()
	}

	if err != nil {
		printErrorAndExit(fmt.Errorf("%s (%v)", out, err))
	}

	fmt.Println("Create new Linux user")
	fmt.Println(userName)
	fmt.Println(userPassword)
}

func removeUser(userName string) error {
	userDelPath, _ := exec.LookPath("userdel")
	if err := exec.Command(userDelPath, userName).Run(); err != nil {
		return fmt.Errorf("Can't remove user: %v", err)
	}
	fmt.Printf("Linux user \"%s\" deleted \n", userName)
	return nil
}

func getUser(userName string) *user.User {
	linuxUser, err := user.Lookup(userName)
	if err != nil {
		// fmt.Println(err)
		linuxUser = nil
	}
	return linuxUser
}

// AddUser is create user in linux system with chroot
func addUser(domainName, userName, sitesPath string, sftp bool) (uid, gid int) {
	var err error

	// If userName is not set, then the userName will be equal to domainName
	if userName == "" {
		re := regexp.MustCompile("^[\\d]+|[^a-zA-Z0-9-]")
		userName = re.ReplaceAllString(domainName, "")
	}

	//check user exist
	if linuxUser := getUser(userName); linuxUser != nil {
		fmt.Println("This user exists")
		uid, _ = strconv.Atoi(linuxUser.Uid)
		gid, _ = strconv.Atoi(linuxUser.Gid)
	} else {
		createUser(domainName, userName, sitesPath, sftp)
		linuxUser := getUser(userName)
		fmt.Println(linuxUser)
		uid, err = strconv.Atoi(linuxUser.Uid)
		if err != nil {
			printErrorAndExit(err)
		}
		gid, err = strconv.Atoi(linuxUser.Gid)
		if err != nil {
			printErrorAndExit(err)
		}
	}
	fmt.Printf("uid=%v\ngid=%v\n", uid, gid)
	return
}

// CreateFile is a simple function to create a file and write to it
func createFile(file, content string) error {
	f, err := os.Create(file)
	if err != nil {
		return fmt.Errorf("Can't create file: %v", err)
	}

	if _, err = f.WriteString(content + "\n"); err != nil {
		return fmt.Errorf("Can't Write to file: %v", err)
	}

	f.Sync()
	f.Close()

	return nil
}

// RemovePath is a simple function to remove a file
func removePath(path string) error {
	if err := os.RemoveAll(path); err != nil {
		return fmt.Errorf("Can't delete to file or directory: %v", err)
	}
	return nil
}

// CreateSymlink is a simple function to add a symbolic link to the file
func createSymlink(oldname, newname string) {
	err := os.Symlink(oldname, newname)
	if err != nil {
		panic(err)
	}
}

// AddSiteFolders function to create a site folder
func addSiteFolders(domainName string, uid, gid int) {
	fmt.Println("Create folders for site")
	// home dir perm is root
	createDir("/var/www/" + domainName)
	// Dir for site
	createDir("/var/www/" + domainName + "/www")
	// Dir for temp
	createDir("/var/www/" + domainName + "/tmp")
	// Dir for logs
	createDir("/var/www/" + domainName + "/log")
	// Dir for ssh keys
	createDir("/var/www/" + domainName + "/.ssh")

	// Chown dirs in up
	os.Chown("/var/www/"+domainName+"/www", uid, gid)
	os.Chown("/var/www/"+domainName+"/tmp", uid, gid)
	os.Chown("/var/www/"+domainName+"/log", uid, gid)
	os.Chown("/var/www/"+domainName+"/.ssh", uid, gid)
}

// StdChars it is symbols to be used in the password
var stdChars = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&()-_=+?/{}[]~")

// NewPassword generates new password
func newPassword(length int) string {
	return randСhar(length, stdChars)
}

func randСhar(length int, chars []byte) string {
	newPword := make([]byte, length)
	randomData := make([]byte, length+(length/4)) // storage for random bytes.
	clen := byte(len(chars))
	maxrb := byte(256 - (256 % len(chars)))
	i := 0
	for {
		if _, err := io.ReadFull(rand.Reader, randomData); err != nil {
			panic(err)
		}
		for _, c := range randomData {
			if c >= maxrb {
				continue
			}
			newPword[i] = chars[c%clen]
			i++
			if i == length {
				return string(newPword)
			}
		}
	}
}
