package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"runtime"

	"github.com/imdario/mergo"

	"gopkg.in/gcfg.v1"
)

// Options is struct for config and options
type Options struct {
	Domain      string
	User        string
	SitesPath   string
	DNS         string
	Token       string
	SSHKeyPath  string
	WebServer   string
	IPv4        string
	IPv6        string
	Sftp        bool
	Letsencrypt bool
	HTTP2       bool
}

var (
	configpath, subdomain string

	config struct {
		Webhelper Options
	}

	options Options
)

func usage() {
	fmt.Printf(`WebHelper is a tool for initial configuration of your website.
See http://github.com/xdevm/webhelper/README.md for information about getting started.

usage: webhelper <command> [common options] [command options]

Commands:
  addsite       Adds the website to server.
  delsite       Removes a website from server.
  help          Print this help text.
  init          Create an initial webhelper.config configuration file.
                (You will need to edit it before using webhelper)

Common options:
  -d, --domain <name>           Domain of website.
  -sd, --subdomain <name>       Subdomain of website.
  -u, --user <name>             User for website. If you don't specify,
                                it will use the domain name [a-z0-9-].
  -p, --path <folder>           The path to the folder with sites.
  -cp, --configpath <name>      Name of configuration file.
  -t, --token <token>           Token for DNS service.
  --dns <service>               DNS service (yandex)
  -ws, --webserver <name>       Web-server (nginx,apache)
  --sftp                        Use sftp access to website files.
  -le, --letsencrypt            Create encryption using let's encrypt.

Option valid for "siteadd":
  -h, --http2                   Enable Protocol http2.
  -4, --ipv4 X.X.X.X            Use this IPv4 adress
                                (If not selected it will automatically detect)
  -6, --ipv6 X:X:X:X:X:X:X:X    Use this IPv6 adress
                                (If not selected it will automatically detect)

  -spkp, --sshkeypath <file>    The path to the file with polichnym key.
                                (If enable sftp)

DNS services
ya, yandex      Yandex Mail for domain (https://pdd.yandex.ru/)
`)
}
func shortUsage() {
	fmt.Fprintf(os.Stderr, `usage: webhelper [common options] <command> [command options]
Supported commands are:ErrInvalid
  siteadd   Adds the website to server.
  sitedel   Removes a website from server.
  init      Create an initial webhelper configuration file
'webhelper help' prints more detailed documentation.
`)
}

func printErrorAndExit(err error) {
	fmt.Fprintf(os.Stderr, "\rwebhelper: %s\n", err)
	os.Exit(1)
}

func createConfigFile(filepath string) {
	contents := `; Default webhelper.config file. See
; http://github.com/xdevm/webhelper/README.md for more
; information about setting up webhelper.
[webhelper]
;domain = example.com
;user = user
;dns = yandex
;token =
;sitesPath = /var/www
;SSHKeyPath = ~/.ssh/id_rsa.pub
;webServer = nginx
;IPv4 =
;IPv6 =
;sftp = false
;letsencrypt = false
;http2 = false
`
	// Don't overwrite an already-existing configuration file.
	if _, err := os.Stat(filepath); os.IsNotExist(err) {
		err := ioutil.WriteFile(filepath, []byte(contents), 0600)
		if err != nil {
			printErrorAndExit(fmt.Errorf("%s: %v", filepath, err))
		}
		fmt.Printf("created configuration file %s.\n", filepath)
	} else {
		printErrorAndExit(fmt.Errorf("%s: file already exists; "+
			"leaving it alone.", filepath))
	}
}

func readConfigFile(filepath string) {
	if runtime.GOOS != "windows" {
		if info, err := os.Stat(filepath); err != nil {
			printErrorAndExit(fmt.Errorf("%s: %v", filepath, err))
		} else if goperms := info.Mode() & ((1 << 6) - 1); goperms != 0 {
			printErrorAndExit(fmt.Errorf("%s: permissions of configuration file "+
				"allow group/other access. Your secrets are at risk.",
				filepath))
		}
	}

	err := gcfg.ReadFileInto(&config, filepath)
	if err != nil {
		printErrorAndExit(fmt.Errorf("%s: %v. (You may want to run \"webhelper "+
			"init\" to create an initial configuration file.)", filepath, err))
	}
}

func main() {
	cmdLine := flag.NewFlagSet("", flag.ExitOnError)

	cmdLine.StringVar(&options.Domain, "domain", "", "domain name to perform actions")
	cmdLine.StringVar(&options.Domain, "d", "", "domain name to perform actions (shorthand)")

	cmdLine.StringVar(&subdomain, "subdomain", "", "subdomain name to perform actions")
	cmdLine.StringVar(&subdomain, "sd", "", "subdomain name to perform actions (shorthand)")

	cmdLine.StringVar(&options.User, "user", "", "user name to perform actions")
	cmdLine.StringVar(&options.User, "u", "", "user name to perform actions (shorthand)")

	cmdLine.StringVar(&options.SitesPath, "path", "/var/www", "The path to the folder with sites")
	cmdLine.StringVar(&options.SitesPath, "p", "/var/www", "The path to the folder with sites (shorthand)")

	cmdLine.StringVar(&configpath, "configpath", "webhelper.config", "The path to the configuration file")
	cmdLine.StringVar(&configpath, "c", "webhelper.config", "The path to the configuration file (shorthand)")

	cmdLine.StringVar(&options.SSHKeyPath, "sshkeypath", "", "The path to the public key")
	cmdLine.StringVar(&options.SSHKeyPath, "s", "", "The path to the public key")

	cmdLine.StringVar(&options.Token, "token", "", "Token for DNS service.")
	cmdLine.StringVar(&options.Token, "t", "", "Token for DNS service.")

	cmdLine.StringVar(&options.DNS, "dns", "", "DNS service [yandex].")

	cmdLine.StringVar(&options.WebServer, "webserver", "", "web-server [nginx,apache].")
	cmdLine.StringVar(&options.WebServer, "w", "", "web-server [nginx,apache].")

	cmdLine.StringVar(&options.IPv4, "ipv4", "", "Use this IPv4 adress")
	cmdLine.StringVar(&options.IPv6, "4", "", "Use this IPv4 adress")

	cmdLine.StringVar(&options.IPv4, "ipv6", "", "Use this IPv6 adress")
	cmdLine.StringVar(&options.IPv6, "6", "", "Use this IPv6 adress")

	cmdLine.BoolVar(&options.Sftp, "sftp", false, "sftp user")

	cmdLine.BoolVar(&options.Letsencrypt, "letsencrypt", false, "Use let's encrypt")
	cmdLine.BoolVar(&options.Letsencrypt, "l", false, "Use let's encrypt")

	cmdLine.BoolVar(&options.Letsencrypt, "http2", false, "Use http2")
	cmdLine.BoolVar(&options.Letsencrypt, "h", false, "Use http2")

	// Print usage and exit if no arguments
	if len(os.Args) == 1 {
		shortUsage()
		os.Exit(0)
	}

	// Parse flags
	cmdLine.Parse(os.Args[2:])

	// Parse command
	cmd := os.Args[1]

	switch cmd {
	case "init":
		createConfigFile(configpath)
		return
	case "help":
		usage()
		return
	}

	readConfigFile(configpath)

	// Merge flags and config
	mergo.Merge(&options, config.Webhelper)

	//Check domain name
	if options.Domain == "" {
		printErrorAndExit(fmt.Errorf("Please provide domain name using -d or --domain"))
	}

	// Check root privileges
	if checkSuperUser() {
		printErrorAndExit(fmt.Errorf("Program needs root privileges"))
	}

	switch cmd {
	case "siteadd":
		siteadd()
	case "sitedel":
		sitedel()
	default:
		printErrorAndExit(fmt.Errorf("Not found command"))
	}
}
